<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\FranchiseMenuController;
use App\Http\Controllers\Api\PacketFacilityController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin', 'middleware' => ['auth:api']], function () {
    Route::apiResource('fasilitas', App\Http\Controllers\Api\FacilityController::class);
    Route::apiResource('paket', App\Http\Controllers\Api\PacketController::class);
    Route::apiResource('menu', App\Http\Controllers\Api\MenuController::class);
    Route::apiResource('franchise', App\Http\Controllers\Api\FranchiseController::class);
    Route::apiResource('pemilik', App\Http\Controllers\Api\OwnerController::class);
    Route::apiResource('menu', App\Http\Controllers\Api\MenuController::class);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::get('profile', [AuthController::class, 'profile']);
    Route::post('addfacility', [PacketFacilityController::class, 'store']);
    Route::delete('deletefacility/{packetFacilityId}', [PacketFacilityController::class, 'destroy']);
    Route::post('addmenu', [FranchiseMenuController::class, 'store']);
    Route::delete('deletemenu/{franchiseMenuId}', [FranchiseMenuController::class, 'destroy']);
});


Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::get('fasilitas', [App\Http\Controllers\Api\FacilityController::class, 'index']);
Route::get('paket', [App\Http\Controllers\Api\PacketController::class, 'index']);
Route::get('menu', [App\Http\Controllers\Api\MenuController::class, 'index']);
Route::get('franchise', [App\Http\Controllers\Api\FranchiseController::class, 'index']);
Route::get('pemilik', [App\Http\Controllers\Api\OwnerController::class, 'index']);

Route::get('fasilitas/{FacilityId}', [App\Http\Controllers\Api\FacilityController::class, 'show']);
Route::get('paket/{PacketId}', [App\Http\Controllers\Api\PacketController::class, 'show']);
Route::get('menu/{MenuId}', [App\Http\Controllers\Api\MenuController::class, 'show']);
Route::get('franchise/{FranchiseId}', [App\Http\Controllers\Api\FranchiseController::class, 'show']);
Route::get('pemilik/{PemilikId}', [App\Http\Controllers\Api\OwnerController::class, 'show']);
