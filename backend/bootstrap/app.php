<?php

use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


return Application::configure(basePath: dirname(__DIR__))
    ->withRouting(
        web: __DIR__ . '/../routes/web.php',
        api: __DIR__ . '/../routes/api.php',
        commands: __DIR__ . '/../routes/console.php',
        health: '/up',
    )
    ->withMiddleware(function (Middleware $middleware) {
        //
    })
    ->withExceptions(function (Exceptions $exceptions) {

        $exceptions->renderable(function (NotFoundHttpException $e, $request) {
            return response()->json([
                'status' => "error",
                'data'    => null,
                'message' => $e->getMessage(),
            ], Response::HTTP_NOT_FOUND);
        });
        $exceptions->renderable(function (AuthenticationException $e, $request) {
            return response()->json([
                'status' => "error",
                'data'    => null,
                'message' => $e->getMessage(),
            ], Response::HTTP_UNAUTHORIZED);
        });
    })->create();
