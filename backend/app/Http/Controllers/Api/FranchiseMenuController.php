<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\FranchiseMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FranchiseMenuController extends Controller
{

    public function store(Request $request)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'franchise_id'     => ['required'],
            'menu_id'     => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $franchiseMenu = FranchiseMenu::create([
            'franchise_id'     => $request->franchise_id,
            'menu_id'     => $request->menu_id,
        ]);

        //return response
        return response()->json($franchiseMenu);
    }

    
    public function destroy($franchiseMenuId)
    {
        $franchiseMenu = FranchiseMenu::find($franchiseMenuId);
        $franchiseMenu->delete();
        return response()->json(["status" => "OK"]);
    }
}
