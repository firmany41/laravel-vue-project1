<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PacketFacility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PacketFacilityController extends Controller
{
    public function store(Request $request)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'packet_id'     => ['required'],
            'facility_id'     => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $packetFacility = PacketFacility::create([
            'packet_id'     => $request->packet_id,
            'facility_id'     => $request->facility_id,
        ]);

        //return response
        return response()->json($packetFacility);
    }

    public function destroy($packetFacilityId)
    {
        $packetFacility = PacketFacility::find($packetFacilityId);
        $packetFacility->delete();
        return response()->json(["status" => "OK"]);
    }
}
