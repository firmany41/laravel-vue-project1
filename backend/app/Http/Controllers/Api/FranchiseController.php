<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FranchiseResource;
use App\Models\Franchise;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FranchiseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $franchise = Franchise::with('owner','franchiseMenus')->get();

        return new FranchiseResource(true, 'List Data Franchise', $franchise);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'name'     => ['required'],
            'address'   => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //create franchise
        $franchise = Franchise::create([
            'name'     => $request->name,
            'address'   => $request->address,
        ]);

        //return response
        return new FranchiseResource(true, 'Data Franchise Berhasil Ditambahkan!', $franchise);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $franchise = Franchise::find($id);

        return new FranchiseResource(true, 'Detail Data Franchise!', $franchise);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'name'     => ['required'],
            'address'   => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //find franchise by ID
        $franchise = Franchise::find($id);

        //update franchise
        $franchise->update([
            'name'     => $request->name,
            'address'   => $request->address,
        ]);

        //return response
        return new FranchiseResource(true, 'Data Franchise Berhasil Diubah!', $franchise);
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $franchise = Franchise::find($id);
        $franchise->delete();
        return new FranchiseResource(true, 'Data Franchise Berhasil Dihapus!', null);
    }
}
