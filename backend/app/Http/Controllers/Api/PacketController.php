<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PacketResource;
use App\Models\Facility;
use App\Models\Packet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PacketController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $packet = Packet::with('packetFacilities')->orderby('id', 'ASC')->get();
        return new PacketResource(true, 'List Data Paket', $packet);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'name'     => ['required'],
            'image'     => ['required', 'image', 'max:2048'],
            'description'   => ['required'],
            'price'     => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //create packet
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('packets');
            $packet = Packet::create([
                'name'     => $request->name,
                'image'     => $image,
                'description'   => $request->description,
                'price'     => $request->price,
            ]);

            //return response
            return new PacketResource(true, 'Data Paket Berhasil Ditambahkan!', $packet);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $packet = Packet::find($id);

        return new PacketResource(true, 'Detail Data Paket!', $packet);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'name'     => ['required'],
            'image'     => ['image', 'max:2048'],
            'description'   => ['required'],
            'price'     => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //find facility by ID
        $packet = Packet::find($id);

        if ($request->hasFile('image')) {
            Storage::delete('packets/' . basename($packet->image));
            $image = $request->file('image')->store('packets');

            //create packet
            $packet->update([
                'name'     => $request->name,
                'image'     => $image,
                'description'   => $request->description,
                'price'     => $request->price,
            ]);
        } else {
            $packet->update([
                'name'     => $request->name,
                'description'   => $request->description,
                'price'     => $request->price,
            ]);
        }

        //return response
        return new PacketResource(true, 'Data Paket Berhasil Diubah!', $packet);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $packet = Packet::find($id);
        Storage::delete('packets/' . basename($packet->image));
        $packet->delete();
        return new PacketResource(true, 'Data Paket Berhasil Dihapus!', null);
    }
}
