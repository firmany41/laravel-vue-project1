<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OwnerResource;
use App\Models\Owner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $owner = Owner::with('franchise')->get();

        return new OwnerResource(true, 'List Data Pemilik', $owner);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'franchise_id'   => ['required'],
            'name'     => ['required'],
            'phone'     => ['required', 'numeric'],
            'email'     => ['required', 'email'],
            'address'     => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //create owner
        $owner = Owner::create([
            'franchise_id'   => $request->franchise_id,
            'name'     => $request->name,
            'phone'   => $request->phone,
            'email'   => $request->email,
            'address'   => $request->address,
        ]);

        //return response
        return new OwnerResource(true, 'Data Pemilik Berhasil Ditambahkan!', $owner);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $owner = Owner::find($id);

        return new OwnerResource(true, 'Detail Data Pemilik!', $owner);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'franchise_id'   => ['required'],
            'name'     => ['required'],
            'phone'     => ['required', 'numeric'],
            'email'     => ['required', 'email'],
            'address'     => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //find owner by ID
        $owner = Owner::find($id);

        //update owner
        $owner->update([
            'franchise_id'   => $request->franchise_id,
            'name'     => $request->name,
            'phone'   => $request->phone,
            'email'   => $request->email,
            'address'   => $request->address,
        ]);

        //return response
        return new OwnerResource(true, 'Data Pemilik Berhasil Diubah!', $owner);
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $owner = Owner::find($id);
        $owner->delete();
        return new OwnerResource(true, 'Data Pemilik Berhasil Dihapus!', null);
    }
}
