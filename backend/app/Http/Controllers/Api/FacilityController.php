<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FacilityResource;
use App\Models\Facility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $facility = Facility::orderby('id', 'ASC')->paginate();

        return new FacilityResource(true, 'List Data Fasilitas', $facility);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'name'     => ['required'],
            'description'   => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //create facility
        $facility = Facility::create([
            'name'     => $request->name,
            'description'   => $request->description,
        ]);

        //return response
        return new FacilityResource(true, 'Data Fasilitas Berhasil Ditambahkan!', $facility);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $facility = Facility::find($id);

        return new FacilityResource(true, 'Detail Data Fasilitas!', $facility);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'name'     => ['required'],
            'description'   => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //find facility by ID
        $facility = Facility::find($id);

        //update facility
        $facility->update([
            'name'     => $request->name,
            'description'   => $request->description,
        ]);

        //return response
        return new FacilityResource(true, 'Data Fasilitas Berhasil Diubah!', $facility);
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $facility = Facility::find($id);
        $facility->delete();
        return new FacilityResource(true, 'Data Fasilitas Berhasil Dihapus!', null);
    }
}
