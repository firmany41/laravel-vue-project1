<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MenuResource;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $menu = Menu::with('franchiseMenus')->orderby('id', 'DESC')->get();
        return new MenuResource(true, 'List Data Menu', $menu);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'name'     => ['required'],
            'image'     => ['required', 'image', 'max:2048'],
            'description'   => ['required'],
            'price'     => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //create menu
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('menus');
            $menu = Menu::create([
                'name'     => $request->name,
                'image'     => $image,
                'description'   => $request->description,
                'price'     => $request->price,
            ]);

            //return response
            return new MenuResource(true, 'Data Menu Berhasil Ditambahkan!', $menu);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $menu = Menu::find($id);

        return new MenuResource(true, 'Detail Data Menu!', $menu);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'name'     => ['required'],
            'image'     => ['image', 'max:2048'],
            'description'   => ['required'],
            'price'     => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //find facility by ID
        $menu = Menu::find($id);

        if ($request->hasFile('image')) {
            Storage::delete('menus/' . basename($menu->image));
            $image = $request->file('image')->store('menus');

            //create menu
            $menu->update([
                'name'     => $request->name,
                'image'     => $image,
                'description'   => $request->description,
                'price'     => $request->price,
            ]);
        } else {
            $menu->update([
                'name'     => $request->name,
                'description'   => $request->description,
                'price'     => $request->price,
            ]);
        }

        //return response
        return new MenuResource(true, 'Data Menu Berhasil Diubah!', $menu);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);
        Storage::delete('menus/' . basename($menu->image));
        $menu->delete();
        return new MenuResource(true, 'Data Menu Berhasil Dihapus!', null);
    }
}
