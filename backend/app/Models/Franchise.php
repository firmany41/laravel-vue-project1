<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Franchise extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function owner()
    {
        return $this->hasOne(Owner::class);
    }
    public function franchiseMenus()
    {
        return $this->hasMany(FranchiseMenu::class)->with('menu');
    }
}
