<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function packetFacilities()
    {
        return $this->hasMany(PacketFacility::class)->with('packet');
    }
}
