<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PacketFacility extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function packet()
    {
        return $this->belongsTo(Packet::class);
    }
    public function facility()
    {
        return $this->belongsTo(Facility::class);
    }
}
