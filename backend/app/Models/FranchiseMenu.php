<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FranchiseMenu extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function franchise()
    {
        return $this->belongsTo(Franchise::class);
    }

    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
}
